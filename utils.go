package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"code.google.com/p/graphics-go/graphics"
)

func generateThumbnail(imagePath string) {
	imageFile, _ := os.Open(imagePath)
	defer imageFile.Close()
	srcImage, _, _ := image.Decode(imageFile)

	dstImage := image.NewRGBA(image.Rect(0, 0, ThumbnailSizeX, ThumbnailSizeY))
	graphics.Thumbnail(dstImage, srcImage)

	dir := filepath.Dir(imagePath)
	newImageName := ThumbnailPrefix + filepath.Base(imagePath)
	newImagePath := filepath.Join(dir, newImageName)
	newImage, _ := os.Create(newImagePath)
	defer newImage.Close()

	jpeg.Encode(newImage, dstImage, &jpeg.Options{jpeg.DefaultQuality})
}

func deleteFromOS(path string) {
	var err = os.Remove(path)
	if isError(err) {
		return
	}

	fmt.Println("==> done deleting file")
}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}

func newfileUploadRequest(uri string, params map[string]string, paramName, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, filepath.Base(path))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return req, err
}
