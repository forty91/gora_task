package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var router *gin.Engine

func main() {
	AutoMigrate()
	router = gin.Default()
	initializePicturesRoutes(router.Group(GaleryPath))
	router.Run(fmt.Sprintf(":%s", ServerPort))
}
