package main

import "github.com/gin-gonic/gin"

func initializePicturesRoutes(router *gin.RouterGroup) {
	router.Static(PicturesPath, PicturesDir)
	router.POST("upload/", createPicture)
	router.GET("pictures/", getPicturesList)
	router.DELETE("pictures/:id", deletePicture)
}
