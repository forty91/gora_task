package main

import (
	"gora_task/common"

	"github.com/jinzhu/gorm"
)

type PictureModel struct {
	gorm.Model
	Name string `json:"name"`
}

func AutoMigrate() {
	var db *gorm.DB

	db = common.GetDB()
	if db == nil {
		db = common.DBInit()
	}

	db.AutoMigrate(&PictureModel{})
}
