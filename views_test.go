package main

import (
	"fmt"
	"gora_task/common"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

var test_db *gorm.DB

func picturesModelMocker() []PictureModel {
	var ret []PictureModel
	for i := 0; i < 3; i++ {
		pictureModel := PictureModel{
			Name: fmt.Sprintf("test_picture%v", i),
		}
		test_db.Create(&pictureModel)
		ret = append(ret, pictureModel)
	}
	return ret
}

func resetDBWithMock() {
	common.TestDBFree(test_db)
	test_db = common.TestDBInit()
	AutoMigrate()
	picturesModelMocker()
}

func TestCreatePicture(t *testing.T) {
	test_db = common.TestDBInit()
	resetDBWithMock()

	asserts := assert.New(t)
	r := gin.New()
	initializePicturesRoutes(r.Group(GaleryPath))

	generateTestImage()
	defer deleteFromOS(testImagePath)
	defer deleteFromOS(testImageThumbnailPath)

	req, _ := newfileUploadRequest("/galery/upload/", nil, "file", testImagePath)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	asserts.Equal(w.Code, http.StatusCreated)
	asserts.Regexp(fmt.Sprintf("Picture '%s' uploaded successfully!", filepath.Base(testImagePath)), w.Body.String())
}

func TestGetPicturesList(t *testing.T) {
	test_db = common.TestDBInit()
	resetDBWithMock()

	asserts := assert.New(t)
	r := gin.New()
	initializePicturesRoutes(r.Group(GaleryPath))

	req, _ := http.NewRequest("GET", "/galery/pictures/", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	asserts.Equal(w.Code, http.StatusOK)
	asserts.Regexp(
		`[
			{
				"id":1,
				"path":"/galery/static/test_picture0",
				"thumbnail_path":"/galery/static/thumbnail_test_picture0"
			},
			{
				"id":2,
				"path":"/galery/static/test_picture1",
				"thumbnail_path":"/galery/static/thumbnail_test_picture1"
			},
			{
				"id":3,
				"path":"/galery/static/test_picture2",
				"thumbnail_path":"/galery/static/thumbnail_test_picture2"
			}
		]`, w.Body.Bytes())
}

func TestDeletePicture(t *testing.T) {
	test_db = common.TestDBInit()
	resetDBWithMock()

	asserts := assert.New(t)
	r := gin.New()
	initializePicturesRoutes(r.Group(GaleryPath))

	req, _ := http.NewRequest("DELETE", "/galery/pictures/1", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	asserts.Equal(w.Code, http.StatusOK)
	asserts.Regexp("Picture 1 deleted successfully!", w.Body.String())
}
