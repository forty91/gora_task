package main

type PictureSerializer struct {
	ID            uint   `json:"id"`
	Path          string `json:"path"`
	ThumbnailPath string `json:"thumbnail_path"`
}
