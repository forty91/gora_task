package main

import (
	"fmt"
	"gora_task/common"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

func createPicture(c *gin.Context) {
	var picture PictureModel

	file, err := c.FormFile("file")
	if err != nil {
		log.Fatal(err)
	}

	db := common.GetDB()
	db.Where("name = ?", file.Filename).First(&picture)
	if picture.ID != 0 {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"status":  http.StatusBadRequest,
				"message": fmt.Sprintf("Picture with name: %s already exists!", file.Filename)})
		return
	}

	filePath := filepath.Join(PicturesDir, file.Filename)
	err = c.SaveUploadedFile(file, filePath)
	if err != nil {
		log.Fatal(err)
	}
	generateThumbnail(filePath)

	picture = PictureModel{
		Name: file.Filename,
	}
	db.Save(&picture)

	c.JSON(
		http.StatusCreated,
		gin.H{
			"status":    http.StatusCreated,
			"message":   fmt.Sprintf("Picture '%s' uploaded successfully!", file.Filename),
			"pictureId": picture.ID})
}

func getPicturesList(c *gin.Context) {
	var pictures []PictureModel
	var serializers []PictureSerializer

	db := common.GetDB()
	db.Find(&pictures)

	for _, item := range pictures {
		serializers = append(
			serializers, PictureSerializer{
				ID:            item.ID,
				Path:          filepath.Join(GaleryPath, PicturesPath, item.Name),
				ThumbnailPath: filepath.Join(GaleryPath, PicturesPath, ThumbnailPrefix+item.Name),
			})
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"status": http.StatusOK,
			"data":   serializers})
}

func deletePicture(c *gin.Context) {
	var picture PictureModel
	pictureID := c.Param("id")
	db := common.GetDB()
	db.First(&picture, pictureID)

	if picture.ID == 0 {
		c.JSON(
			http.StatusNotFound,
			gin.H{
				"status":  http.StatusNotFound,
				"message": fmt.Sprintf("Picture %s not exists!", pictureID)})
		return
	}
	db.Delete(&picture)

	deleteFromOS(filepath.Join(PicturesDir, (&picture).Name))
	deleteFromOS(filepath.Join(PicturesDir, ThumbnailPrefix+(&picture).Name))

	c.JSON(
		http.StatusOK,
		gin.H{
			"status":  http.StatusOK,
			"message": fmt.Sprintf("Picture %s deleted successfully!", pictureID)})
}
