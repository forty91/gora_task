package main

const (
	ServerPort = "8081"

	ThumbnailPrefix = "thumbnail_"
	ThumbnailSizeX  = 100
	ThumbnailSizeY  = 100

	GaleryPath = "/galery/"

	PicturesDir  = "/var/www/gora"
	PicturesPath = "/static"
)
