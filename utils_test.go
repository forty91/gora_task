package main

import (
	"fmt"
	"image"
	"image/jpeg"
	"os"
	"path/filepath"
	"testing"
)

const testImagePath = "/tmp/test_image.jpg"

var testImageThumbnailPath = filepath.Join(
	filepath.Dir(testImagePath),
	ThumbnailPrefix+filepath.Base(testImagePath))

func generateTestImage() {
	testImageFile, _ := os.Create(testImagePath)
	defer testImageFile.Close()
	img := image.NewGray(image.Rect(0, 0, 1000, 1000))
	jpeg.Encode(testImageFile, img, &jpeg.Options{jpeg.DefaultQuality})
}

func getImageDimension(imagePath string) (int, int) {
	file, err := os.Open(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	image, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", imagePath, err)
	}
	return image.Width, image.Height
}

func TestDeleteFromOS(t *testing.T) {
	generateTestImage()
	deleteFromOS(testImagePath)

	if _, err := os.Stat(testImagePath); err == nil {
		t.Fail()
	}
}

func TestGenerateThumbnail(t *testing.T) {
	generateTestImage()
	generateThumbnail(testImagePath)
	defer deleteFromOS(testImagePath)
	defer deleteFromOS(testImageThumbnailPath)

	width, height := getImageDimension(testImageThumbnailPath)
	if width != ThumbnailSizeX || height != ThumbnailSizeY {
		t.Fail()
	}
}
